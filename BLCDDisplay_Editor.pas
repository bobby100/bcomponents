unit BLCDDisplay_Editor;

{$mode objfpc}{$H+}

interface

uses        
  Classes, SysUtils, PropEdits, ComponentEditors, 
  BLCDDisplay;

type
  TBLCDDisplayCharDefsPropertyEditor = class(TPersistentPropertyEditor)
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetAttributes: TPropertyAttributes; override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
    function BLCDDisplay: TBLCDDisplay;
  end;
  
  TBLCDDisplayComponentEditor = class(TComponentEditor)
  private
    procedure EditLines;    
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;    
    function GetVerbCount: Integer; override;
    function BLCDDisplay: TBLCDDisplay;
  end;

procedure EditCharDefs(ABLCDDisplay: TBLCDDisplay);
  
implementation

uses
  Controls, StdCtrls, Dialogs, ButtonPanel, Forms,
  BLCDDisplay_EditorForm;

{ Opens the char def editor. }
procedure EditCharDefs(ABLCDDisplay: TBLCDDisplay);
var
  F: TBLCDCharDefsEditor;
begin
  F := TBLCDCharDefsEditor.Create(nil);
  try
    F.Position := poScreenCenter;
    F.BLCDDisplay := TBLCDDisplay(ABLCDDisplay);
    F.ShowModal;  // Cancel has been handled by the editor form.
  finally
    F.Free;
  end;
end;

{ Loads the char defs of the specified BLCDDisplay from an xml file. }
procedure LoadCharDefsFromFile(ABLCDDisplay: TBLCDDisplay);
var
  dlg: TOpenDialog;
begin
  dlg := TOpenDialog.Create(nil);
  try
    dlg.FileName := '';
    dlg.Filter := 'XML files (*.xml)|*.xml';
    if dlg.Execute then
    begin
      ABLCDDisplay.CharDefs.LoadFromFile(dlg.FileName);
      ABLCDDisplay.Invalidate;
    end;
  finally
    dlg.Free;
  end;
end;
  
{ Saves the chardefs of the specified BLCDDisplay to an xml file. }
procedure SaveCharDefsToFile(ABLCDDisplay: TBLCDDisplay);
var
  dlg: TOpenDialog;
begin
  dlg := TSaveDialog.Create(nil);
  try
    dlg.FileName := '';
    dlg.Filter := 'XML files (*.xml)|*.xml';
    if dlg.Execute then
      ABLCDDisplay.CharDefs.SaveToFile(dlg.FileName);
  finally
    dlg.Free;
  end;
end;


{ TBLCDDisplayCharDefsPropertyEditor }

{ Opens the chardefs editor. }
procedure TBLCDDisplayCharDefsPropertyEditor.Edit;
begin
  EditCharDefs(BLCDDisplay);
end;

{ Executes the routines assigned to the CharDefs context menu }
procedure TBLCDDisplayCharDefsPropertyEditor.ExecuteVerb(Index: Integer); 
begin
  case Index of
    0: Edit;
    1: LoadCharDefsFromFile(BLCDDisplay);
    2: SaveCharDefsToFile(BLCDDisplay);
  end;
end;

{ The property editor should open the CharDefs editor. }
function TBLCDDisplayCharDefsPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited GetAttributes + [paDialog];
end;

{ Determines how many items will be added to the CharDefs context menu. }
function TBLCDDisplayCharDefsPropertyEditor.GetVerbCount: Integer; 
begin
  Result := 3;
end;

{ Determines the menu item text for CharDefs context menu. }
function TBLCDDisplayCharDefsPropertyEditor.GetVerb(Index: Integer): string; 
begin
  case Index of
    0: Result := 'Edit...';
    1: Result := 'Load from file...';
    2: Result := 'Save to file...';
  end;
end;

function TBLCDDisplayCharDefsPropertyEditor.BLCDDisplay: TBLCDDisplay;
begin
  Result := TBLCDDisplay(GetComponent(0));
end;


{ TBLCDDisplayComponentEditor }

procedure TBLCDDisplayComponentEditor.Edit;
begin
  ExecuteVerb(0);
end;

procedure TBLCDDisplayComponentEditor.EditLines;
var
  F: TForm;
  Memo: TMemo;
begin
  F := TForm.CreateNew(nil);
  try
    F.Caption := 'Edit BLCDDisplay text';
    F.Position := poScreenCenter;
    F.Width := 300;
    F.Height := 200;
    Memo := TMemo.Create(F);
    with Memo do
    begin
      Align := alClient;
      BorderSpacing.Around := 8;
      Parent := F;
      Lines.Assign(BLCDDisplay.Lines);
    end;
    with TButtonPanel.Create(F) do
    begin
      ShowButtons := [pbOK, pbCancel];
      Parent := F;
    end;
    if F.ShowModal = mrOK then
    begin
      BLCDDisplay.Lines.Assign(Memo.Lines);
      BLCDDisplay.Invalidate;
    end;
  finally
    F.Free;
  end;
end;

procedure TBLCDDisplayComponentEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0: EditLines;
    1: EditCharDefs(BLCDDisplay);
    2: LoadCharDefsFromFile(BLCDDisplay);
    3: SaveCharDefsToFile(BLCDDisplay);
  end;
end;

{ Determines how many items will be added to the BLCDDisplay context menu. }
function TBLCDDisplayComponentEditor.GetVerbCount: Integer; 
begin
  Result := 4;
end;

{ Determines the menu item text for BLCDDisplay context menu. }
function TBLCDDisplayComponentEditor.GetVerb(Index: Integer): string; 
begin
  case Index of
    0: Result := 'Lines text...';
    1: Result := 'Edit character defs...';
    2: Result := 'Load character defs from file...';
    3: Result := 'Save character defs to file...';
  end;
end;

function TBLCDDisplayComponentEditor.BLCDDisplay: TBLCDDisplay;
begin
  Result := TBLCDDisplay(GetComponent);
end;

end.

