{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit BComponents;

{$warn 5023 off : no warning about unused units}
interface

uses
  BRingSlider, BSelector, BLED, BLCDDisplay, BComponentsRegister, 
  BComponentsTheme, BComponentsTypes, BQLED, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('BComponentsRegister', @BComponentsRegister.Register);
end;

initialization
  RegisterPackage('BComponents', @Register);
end.
