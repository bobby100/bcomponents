# BComponents

Lazarus graphical components. The package contains 5 components at the moment:


## BRingSlider

A potentiometer knob

## BSelector

A selector knob

## BLED

A LED or a switch

## BQLED

A square LED or a switch

## BLCDDisplay

Based on [TLCDDisplay](https://wiki.freepascal.org/Industrial#TLCDDisplay) from IndustrialStuff package.
Converted to use BGRABitmap and added graphical effects

## Screenshot

![Demo](graphics/demo.png)  
![Demo_LCD](graphics/demo_LCD.png)

## Theme examples  
Plastic morph:

![plastic_morph](graphics/demo_plastic_morph.png)

Alien:

![alien](graphics/demo_alien.png)



# ThemeBuilder

App for editing themes:

![ThemeBuilder](graphics/ThemeBuilder.png)



# Credits  
BComponents are using BGRABitmap package: https://github.com/bgrabitmap/bgrabitmap  
Credits to the nice people on the Lazarus forum, who inspired or helped me coding this components: wp, hedgehog, circular, lainz, alpine
Some of the component icons: [Graffiti icons created by Vitaly Gorbachev - Flaticon](https://www.flaticon.com/free-icons/graffiti)