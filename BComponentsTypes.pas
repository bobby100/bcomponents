{
 *****************************************************************************
  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

 Author: Boban Spasic
}

unit BComponentsTypes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TZStyle = (fsFlat, fsLowered, fsRaised);
  TDotShape = (stSquare, stRound);
  TColorScheme = (csCustom, csBlue, csGreen, csInvGreen);
  TBoardShadow = (bsNone, bsOwn, bsFrame);

implementation

end.

