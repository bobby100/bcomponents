
{**********************************************************************
 Package BComponents
***********************************************************************}

unit BComponentsRegister;

interface

uses
  Classes, LResources, BSelector, BRingSlider, BLED,
  BLCDDisplay, BLCDDisplay_Editor, BComponentsTheme, BQLED;

procedure Register;

implementation

uses
  PropEdits, ComponentEditors;

//==========================================================
procedure Register;
begin
  {$I BComponents.lrs}
  RegisterComponents('BComponents', [TBSelector, TBLED, TBQLED, TBRingSlider, TBLCDDisplay, TBComponentsTheme]);
  RegisterPropertyEditor(TypeInfo(TBCharDefs), TBLCDDisplay, 'CharDefs', TBLCDDisplayCharDefsPropertyEditor);
  RegisterComponentEditor(TBLCDDisplay, TBLCDDisplayComponentEditor);
end;

end.
