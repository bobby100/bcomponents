unit untComponentsDemo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, BSelector, BRingSlider, BLED, BComponentsTypes;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    BLED1: TBLED;
    BRingSlider1: TBRingSlider;
    BSelector1: TBSelector;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    rgBSEL: TRadioGroup;
    rgBRS: TRadioGroup;
    rgBLED: TRadioGroup;
    TrackBar1: TTrackBar;
    TrackBar10: TTrackBar;
    TrackBar11: TTrackBar;
    TrackBar12: TTrackBar;
    TrackBar13: TTrackBar;
    TrackBar14: TTrackBar;
    TrackBar15: TTrackBar;
    TrackBar16: TTrackBar;
    TrackBar2: TTrackBar;
    TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    TrackBar5: TTrackBar;
    TrackBar6: TTrackBar;
    TrackBar7: TTrackBar;
    TrackBar8: TTrackBar;
    TrackBar9: TTrackBar;
    procedure FormShow(Sender: TObject);
    procedure SelectorParamChange(Sender: TObject);
    procedure SelectorParamRefresh;
    procedure SliderParamRefresh;
    procedure SliderParamChange(Sender: TObject);
    procedure LEDParamRefresh;
    procedure LEDParamChange(Sender: TObject);
    procedure UpdateHints;
  private

  public

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.FormShow(Sender: TObject);
begin
  SelectorParamRefresh;
  SliderParamRefresh;
  LEDParamRefresh;
  UpdateHints;
end;

procedure TfrmMain.UpdateHints;
var
  i: integer;
begin
  for i := 0 to ControlCount - 1 do
  begin
    if Controls[i] is TTrackBar then
      TTrackBar(Controls[i]).Hint := IntToStr(TTrackBar(Controls[i]).Position);
  end;
end;

procedure TfrmMain.SelectorParamChange(Sender: TObject);
begin
  if Sender = rgBSEL then BSelector1.Style := TZStyle(rgBSEL.ItemIndex);
  if Sender = Checkbox3 then BSelector1.DrawText := CheckBox3.Checked;
  if Sender = Checkbox4 then BSelector1.DrawTextPhong := CheckBox4.Checked;
  if Sender = Checkbox5 then BSelector1.DrawTicks := CheckBox5.Checked;
  if Sender = TrackBar1 then BSelector1.Sensitivity := TrackBar1.Position;
  if Sender = TrackBar2 then BSelector1.MinAngle := TrackBar2.Position;
  if Sender = TrackBar3 then BSelector1.MaxAngle := TrackBar3.Position;
  if Sender = TrackBar4 then BSelector1.MinTicksAngle := TrackBar4.Position;
  if Sender = TrackBar5 then BSelector1.MaxTicksAngle := TrackBar5.Position;
  if Sender = TrackBar6 then BSelector1.TicksCount := TrackBar6.Position;
  if Sender = TrackBar7 then BSelector1.PointerSize := TrackBar7.Position;
  if Sender = TrackBar14 then BSelector1.Altitude := TrackBar14.Position;
  SelectorParamRefresh;
end;

procedure TfrmMain.SelectorParamRefresh;
begin
  rgBSEL.ItemIndex := integer(BSelector1.Style);
  Checkbox3.Checked := BSelector1.DrawText;
  Checkbox4.Checked := BSelector1.DrawTextPhong;
  Checkbox5.Checked := BSelector1.DrawTicks;
  TrackBar1.Position := BSelector1.Sensitivity;
  TrackBar2.Position := BSelector1.MinAngle;
  TrackBar3.Position := BSelector1.MaxAngle;
  TrackBar4.Position := BSelector1.MinTicksAngle;
  TrackBar5.Position := BSelector1.MaxTicksAngle;
  TrackBar6.Position := BSelector1.TicksCount;
  TrackBar7.Position := BSelector1.PointerSize;
  TrackBar14.Position := BSelector1.Altitude;
  UpdateHints;
end;

procedure TfrmMain.SliderParamRefresh;
begin
  rgBRS.ItemIndex := integer(BRingSlider1.Style);
  Checkbox8.Checked := BRingSlider1.DrawText;
  Checkbox9.Checked := BRingSlider1.DrawTextPhong;
  Checkbox10.Checked := BRingSlider1.DrawPointer;
  TrackBar8.Position := BRingSlider1.Sensitivity;
  TrackBar9.Position := BRingSlider1.MinAngle;
  TrackBar10.Position := BRingSlider1.MaxAngle;
  TrackBar11.Position := BRingSlider1.MinValue;
  TrackBar12.Position := BRingSlider1.MaxValue;
  TrackBar15.Position := BRingSlider1.Altitude;
  UpdateHints;
end;

procedure TfrmMain.SliderParamChange(Sender: TObject);
begin
  if Sender = rgBRS then BRingSlider1.Style := TZStyle(rgBRS.ItemIndex);
  if Sender = Checkbox8 then BRingSlider1.DrawText := CheckBox8.Checked;
  if Sender = Checkbox9 then BRingSlider1.DrawTextPhong := CheckBox9.Checked;
  if Sender = Checkbox10 then BRingSlider1.DrawPointer := CheckBox10.Checked;
  if Sender = TrackBar8 then BRingSlider1.Sensitivity := TrackBar8.Position;
  if Sender = TrackBar9 then BRingSlider1.MinAngle := TrackBar9.Position;
  if Sender = TrackBar10 then BRingSlider1.MaxAngle := TrackBar10.Position;
  if Sender = TrackBar11 then BRingSlider1.MinValue := TrackBar11.Position;
  if Sender = TrackBar12 then BRingSlider1.MaxValue := TrackBar12.Position;
  if Sender = TrackBar15 then BRingSlider1.Altitude := TrackBar15.Position;
  SliderParamRefresh;
end;

procedure TfrmMain.LEDParamRefresh;
begin
  rgBLED.ItemIndex := integer(BLED1.Style);
  Checkbox12.Checked := BLED1.Value;
  Checkbox11.Checked := BLED1.Clickable;
  TrackBar13.Position := BLED1.Size;
  TrackBar16.Position := BLED1.Altitude;
  UpdateHints;
end;

procedure TfrmMain.LEDParamChange(Sender: TObject);
begin
  if Sender = Checkbox11 then BLED1.Clickable := CheckBox11.Checked;
  if Sender = rgBLED then BLED1.Style := TZStyle(rgBLED.ItemIndex);
  if Sender = Checkbox12 then BLED1.Value := CheckBox12.Checked;
  if Sender = TrackBar13 then BLED1.Size := TrackBar13.Position;
  if Sender = TrackBar16 then BLED1.Altitude := TrackBar16.Position;
  LEDParamRefresh;
end;

end.

